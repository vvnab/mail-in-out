import { HTMLAttributes } from 'react';
import { useSelector } from 'react-redux';

import styles from 'assets/styles/components/mails/index.module.scss';
import { stopCoverage } from 'v8';

const MailList = ({ children }: HTMLAttributes<HTMLDivElement>) => {
    const store: any = useSelector((store) => store);
    const { mails, loading } = store;
    return (
        <div className={styles.wrap}>
            {loading
                ? 'wait'
                : mails.map(({ name, id }: any) => (
                      <div key={id}>{name}</div>
                  ))}
        </div>
    );
};

export default MailList;
