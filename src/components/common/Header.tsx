import { HTMLAttributes } from 'react';
import Content from 'components/common/Content';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from 'assets/styles/components/common/Header.module.scss';

interface Props extends HTMLAttributes<HTMLDivElement> {
    userName?: string;
    icon: any;
    action: () => void;
}

const Header = ({ userName, icon, action }: Props) => (
    <div className={styles.wrap}>
        <Content className={styles.content}>
            <div className={styles.part}>
                <div className={styles.logo}>КС</div>
            </div>
            <div className={styles.part}>
                <div className={styles.name}>{userName}</div>
                <FontAwesomeIcon
                    icon={icon}
                    className={styles.icon}
                    onClick={() => action()}
                />
            </div>
        </Content>
    </div>
);

export default Header;
