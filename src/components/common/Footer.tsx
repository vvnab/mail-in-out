import { HTMLAttributes } from 'react';
import Content from 'components/common/Content';
import styles from 'assets/styles/components/common/Footer.module.scss';

const Footer = ({}: HTMLAttributes<HTMLDivElement>) => (
    <div className={styles.wrap}>
        <Content className={styles.content}>
            <div>Нордкомп &copy; {new Date().getFullYear()}</div>
            <div><a href="mailto:vvnab@mail.ru">vvnab@mail.ru</a></div>
        </Content>
    </div>
);

export default Footer;
