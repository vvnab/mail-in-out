import { HTMLAttributes } from 'react';
import styles from 'assets/styles/components/common/Content.module.scss';

const Content = ({ children, className }: HTMLAttributes<HTMLDivElement>) => (
    <section className={[styles.wrap, className].join(' ')}>{children}</section>
);

export default Content;
