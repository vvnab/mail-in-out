import { HTMLAttributes } from 'react';
import styles from 'assets/styles/components/common/Layout.module.scss';

const Layout = ({ children }: HTMLAttributes<HTMLDivElement>) => (
    <div className={styles.wrap}>{children}</div>
);

export default Layout;
