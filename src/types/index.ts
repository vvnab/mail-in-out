declare global {
    interface Window {
        grecaptcha: any;
        fbq: any;
    }
}

export * from './user';
export * from './mail';
