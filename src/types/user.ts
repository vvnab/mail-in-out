export interface User {
    id: string;
    name: string;
    email: string;
    super?: true;
}
