export interface Mail {
    id: string;
    type: 'incoming' | 'outcoming';
    datetime: string;
    title: string;
    description?: string;
    owner: string;
    organisation?: string;
    address?: string;
}
