import { Mail } from 'types';

async function request(...rest: any) {
    const response: any = await fetch(rest);
    const json: any = await response.json();
    return json;
}

export async function getMails(filter: any): Promise<Mail[]> {
    return await request('/api/mail');
}
