const initialState = {
    mails: [],
};

export default function combineReducers(state = initialState, action: any) {
    switch (action.type) {
        case 'LOAD_MAILS':
            return {
                ...state,
                loading: true,
            };
        case 'SET_MAILS':
            return {
                ...state,
                mails: action.payload,
                loading: false,
            };
        default:
            return state;
    }
}
