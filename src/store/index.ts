import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const store = createStore(
        rootReducer,
        typeof window !== 'undefined'
            ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(
                  applyMiddleware(sagaMiddleware)
              )
            : applyMiddleware(sagaMiddleware)
    );
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;
