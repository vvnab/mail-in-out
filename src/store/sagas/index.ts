import {
    takeEvery,
    put,
    call,
    fork,
    spawn,
    all,
    delay,
    CallEffect,
    PutEffect,
} from 'redux-saga/effects';

import { getMails } from 'api';

export function* loadUsers(): Generator<any, any, any> {
    yield put({ type: 'LOAD_MAILS' });
    const payload = yield call(getMails, '');
    yield put({ type: 'SET_MAILS', payload });
}

export function* loadBasicDataSaga() {
    yield all([fork(loadUsers)]);
}

export default function* rootSaga() {
    const sagas = [fork(loadBasicDataSaga)];
    yield all(sagas);
}
