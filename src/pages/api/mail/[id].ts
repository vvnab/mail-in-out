import { NextApiRequest, NextApiResponse } from 'next';
import { MongoClient, ObjectId } from 'mongodb';

const uri = 'mongodb://localhost:27017';
const client = new MongoClient(uri);

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { id }: any = req.query;
    const _id = new ObjectId(id);

    await client.connect();
    const db = client.db('mails');
    const ksCollection = db.collection('ks');
    const ncCollection = db.collection('nc');
    let response: any = '';

    switch (req.method) {
        case 'GET':
            response = await ksCollection.findOne({ _id });
            console.log(response);
            break;
        case 'PATCH':
            response = await ksCollection.updateOne(
                { _id },
                { $set: req.body }
            );
            break;
        case 'DELETE':
            response = await ksCollection.deleteOne({ _id });
            break;
        default:
            response = `method ${req.method} not allowed`;
    }

    res.status(200).json(response);
}
