import { NextApiRequest, NextApiResponse } from 'next';
import { MongoClient } from 'mongodb';

const uri = 'mongodb://localhost:27017';
const client = new MongoClient(uri);

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    await client.connect();
    const db = client.db('mails');
    const ksCollection = db.collection('ks');
    const ncCollection = db.collection('nc');
    let response: any = '';

    switch (req.method) {
        case 'POST':
            const postResult = await ksCollection.insertOne(req.body);
            response = postResult;
            break;
        case 'GET':
            const findResult = await ksCollection.find({}).toArray();
            response = findResult.map((i) => ({ ...i, id: i._id }));
            break;
        default:
            response = `method ${req.method} not allowed`;
    }

    res.status(200).json(response);
}
