import ldap from 'ldapjs';
import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

export default NextAuth({
    providers: [
        CredentialsProvider({
            name: 'LDAP',
            credentials: {
                username: { label: 'Имя', type: 'text', placeholder: '' },
                password: { label: 'Пароль', type: 'password' },
            },
            async authorize(credentials, req) {
                // You might want to pull this call out so we're not making a new LDAP client on every login attemp
                const client = ldap.createClient({
                    // @ts-ignore
                    url: process.env.LDAP_URI,
                });
                // Essentially promisify the LDAPJS client.bind function
                return new Promise((resolve, reject) => {
                    const { username, password } = credentials || {
                        username: '',
                        password: '',
                    };
                    console.log(username, password);
                    client.bind('cn=r-abook,ou=robots,ou=taxi,dc=taxi21,dc=ru', 'ABP@ss12', (error: any) => {
                        if (error) {
                            console.error('Failed', error);
                            reject(error);
                        } else {
                            console.log('Logged in');
                            resolve({
                                username,
                                password,
                            });
                        }
                    });
                });
            },
        }),
    ],
    callbacks: {
        async jwt({ token, user }) {
            const isSignIn = user ? true : false;
            if (isSignIn) {
                token.username = user?.username;
                token.password = user?.password;
            }
            return token;
        },
        // @ts-ignore
        async session({ session, token }) {
            return { ...session, user: { name: token.username } };
        },
    },
    secret: process.env.NEXTAUTH_SECRET,
    jwt: {
        secret: process.env.JWT_SECRET,
    },
});
