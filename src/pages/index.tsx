import type { NextPage } from 'next';
import Head from 'next/head';
import { useSession, signIn, signOut } from 'next-auth/react';
import Header from 'components/common/Header';
import Content from 'components/common/Content';
import Footer from 'components/common/Footer';
import MailList from 'components/mails';

import {
    faQuestionCircle as questionIcon,
    faSignOutAlt as exitIcon,
    faBars,
} from '@fortawesome/free-solid-svg-icons';

const fetcher = (...args: any) => fetch(args).then((res) => res.json());

function Component() {
    const { data: session } = useSession();
    if (session) {
        return null;
    }
    return (
        <>
            Not signed in <br />
            <button onClick={() => signIn()}>Sign in</button>
            <hr />
        </>
    );
}

const Home: NextPage = () => {
    const { data: session, status } = useSession();
    const userName = session?.user?.name || '';
    const authenticated = status === 'authenticated';

    return (
        <>
            <Head>
                <title>Регистрация корреспонденции</title>
                <meta
                    name='description'
                    content='Регистрация входящей/исходящей корреспонденции'
                />
                <link rel='icon' href='/favicon.ico' />
            </Head>
            <Header
                userName={userName}
                icon={authenticated ? exitIcon : questionIcon}
                action={() => (authenticated ? signOut() : () => undefined)}
            />
            <Content>
                <Component />
                <MailList />
            </Content>
            <Footer />
        </>
    );
};

export default Home;
